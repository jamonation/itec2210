# setup an apache2 directory to build a docker image
class docker {

  file { '/home/itec2210/apache2':
    ensure => 'directory',
    mode   => '0777',
    owner  => 'root',
    group  => 'root'
  }
  file { '/home/itec2210/apache2/apache2.conf':
    ensure  => 'present',
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/docker/apache2.conf',
    require => File['/home/itec2210/apache2']
  }
  file { '/home/itec2210/apache2/Dockerfile':
    ensure  => 'present',
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/docker/Dockerfile',
    require => File['/home/itec2210/apache2']
  }

}
