class puppetcron {

  cron { 'puppet':
    ensure  => present,
    command => '/usr/bin/puppet agent --test --no-daemonize --server instructor.itec2210.ca --logdest syslog > /dev/null 2>&1',
    user    => 'root',
    minute  => [fqdn_rand(30), fqdn_rand(30) + 30]
  }

  cron { 'remove1':
    ensure  => absent,
    command => '/root/puppet.sh',
    minute  => 0,
    user    => 'root'
  }

  cron { 'remove2':
    ensure  => absent,
    command => '/usr/local/bin/puppet.sh',
    minute  => 0,
    user    => 'root'
  }

}
