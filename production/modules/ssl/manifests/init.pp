# install ssl
class ssl {

  package { 'tshark':
    ensure => 'present'
  }

  file { '/home/itec2210/ssl.pcap':
    ensure  => 'present',
    mode    => '0400',
    owner   => 'itec2210',
    group   => 'itec2210',
    source  => 'puppet:///modules/ssl/ssl.pcap'
  }

}
