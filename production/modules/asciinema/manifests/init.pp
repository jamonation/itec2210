# install asciinema
class asciinema {

  package { 'asciinema':
    ensure => 'present'
  }

  file { '/home/itec2210/postgresql.cast':
    ensure  => 'present',
    mode    => '0400',
    owner   => 'itec2210',
    group   => 'itec2210',
    source  => 'puppet:///modules/asciinema/postgresql.cast'
  }

  file { '/home/itec2210/mattermost.cast':
    ensure  => 'present',
    mode    => '0400',
    owner   => 'itec2210',
    group   => 'itec2210',
    source  => 'puppet:///modules/asciinema/mattermost.cast'
  }


}
