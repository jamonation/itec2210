# install restic, ssh key, and cron to push backups to instructor VM
class restic {

  package { 'restic':
    ensure => 'present'
  }

  sshkey {'instructor.itec2210.ca':
    ensure       => present,
    name         => 'instructor.itec2210.ca',
    host_aliases => '159.203.41.218',
    key          => 'AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBCT5FgtS/KvRyYZCwqMXyHWmnP/ryHi2J1qWpnAKSOiT1of5AHg9NDeetk8a6+xL6bJ5rB6e1EvC6lh4S+bL8No=',
    type         => 'ecdsa-sha2-nistp256'
  }

  file { '/root/.ssh':
    ensure => 'directory',
    mode   => '0700',
    owner  => 'root',
    group  => 'root',
  }

  file { '/root/.ssh/id_rsa':
    ensure  => 'present',
    mode    => '0400',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/restic/id_rsa-restic',
    require => File['/root/.ssh']
  }

  file { '/root/.restic-password':
    ensure  => 'present',
    mode    => '0400',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/restic/password'
  }

  cron { 'restic':
    ensure  => 'present',
    command => 'restic --password-file /root/.restic-password -r sftp:restic@instructor.itec2210.ca:backups backup --tag $(hostname) /opt/mattermost/config',
    minute  => [fqdn_rand(59)],
    hour    => [fqdn_rand(12), fqdn_rand(12) + 12]
  }

}
