node default {

  include docker
  include puppetcron
  include restic
  include asciinema
  include ssl

}
